#include "parser.h"
#include <stdexcept>
#include <regex>

const std::string OPERATORS = "+-*/^%";

Opnode* newNodeFromOperator(char op) {
	switch (op) {
		case '+':
			return new node::Add();
		case '-':
			return new node::Sub();
		case '*':
			return new node::Mult();
		case '/':
			return new node::Div();
		case '^':
			return new node::Pow();
		case '%':
			return new node::Mod();
	}
	throw std::invalid_argument("Character is not an operator");
	return nullptr;
}

std::string parseWhere(std::string s, Context &context) {
	int wherei = s.find(" where ");
	
	if (wherei < 0)
		return s;

	std::string wherec = s.substr(wherei + 7);
	s = s.substr(0, wherei);

	// Strip spaces
	int spi;
	while ((spi = wherec.find(" ")) != -1) {
		wherec = wherec.erase(spi, 1);
	}

	int semicolon = -1;
	do {
		// If there's a semicolon, split the string and parse the first piece
		semicolon = wherec.find(";");
		int end = (semicolon < 0) ? wherec.length() : semicolon;
		std::string statement = wherec.substr(0, end);

		int equal = wherec.find("=");
		// There should be an equal sign here
		if (equal == std::string::npos) {
			throw std::invalid_argument("'=' missing in variable assignment statement");
		}

		// Var name is before the equal sign and value is after.
		std::string varName = statement.substr(0, equal);
		// TODO: Check that variable name doesn't begin with a number and stuff.
		std::string varValueStr = statement.substr(equal+1);
		double varValue = nodeFromStr(varValueStr)->execute(context);
		if (varName.length() == 0 || varValueStr.length() == 0) {
			throw std::invalid_argument("Couldn't understand variable assignment statement");
		}
		context.setVarValue(varName, varValue);

		// Remove parsed part from wherec
		wherec = wherec.substr(semicolon+1);
	} while (semicolon != -1);

	return s;
}

Opnode* checkParenthesis(std::string s) {
	int popen_index = s.find("(");
	if (popen_index == -1) {
		if (s.find(")") != -1) {
			throw std::invalid_argument("Closing parenthesis found but no opening parenthesis is present");
		}
		return nullptr;
	}

	std::string before_par = s.substr(0, popen_index);
	std::string between = s.substr(popen_index + 1);
	std::string after_par;

	int layers = 0;

	for (int i=0; i < between.length(); ++i) {
		if (between[i] == '(') {
			++layers;
		} else if (between[i] == ')') {
			--layers;

			if (layers == -1) {
				after_par = between.substr(i + 1);
				between = between.substr(0, i);

				Opnode* solved_par = nodeFromStr(between);

				char pre_op, post_op;
				pre_op = before_par[before_par.length() - 1];
				if (OPERATORS.find(pre_op) == -1) {
					pre_op = '*';
				} else {
					before_par = before_par.substr(0, before_par.length()-1);
				}

				post_op = after_par[0];
				if (OPERATORS.find(post_op) == -1) {
					post_op = '*';
				} else {
					after_par = after_par.substr(1);
				}

				/*                     ____________
				 *                     |    B     |
				 *                     | operator |
				 *                     |__________|
				 *                      /        \
				 *               ______/_____ ____\______
				 *               |    A     | |   After  |
				 *               | operator | |    ()    |
				 *               |__________| |__________|
				 *                /        \
				 *          _____/____ _____\____
				 *          | Before | | Inside |
				 *          |   ()   | |   ()   |
				 *          |________| |________|
				 */
				Opnode *branchA, *branchB, *result;

				bool pre = before_par.length() > 0;
				bool post = after_par.length() > 0;

				// If we only have what's inside the parenthesis, just
				// parse that
				if (!pre && !post) {
					return solved_par;
				}
				
				if (pre)
					branchA = newNodeFromOperator(pre_op);
				if (post)
					branchB = newNodeFromOperator(post_op);

				// If there's something before the parenthesis, fill
				// node A with (before, between)
				if (pre) {
					branchA->childA = nodeFromStr(before_par);
					branchA->childB = solved_par;
					// And if that's it, return A
					if (!post)
						return branchA;

					// Else, if the operator before comes first, we put A as a child of B
					if (OPERATORS.find(pre_op) > OPERATORS.find(post_op)) {
						branchB->childA = branchA;
						result = branchB;
					// Or if the operator after comes first, we put B as a child of B and
					// our result is branchA
					} else {
						branchB->childA = solved_par;
						branchA->childB = branchB;
						result = branchA;
					}
				} else {
					// Or if only post-par, just the inside
					branchB->childA = solved_par;
					result = branchB;
				}
				// Then the after-par in the other branch of B
				branchB->childB = nodeFromStr(after_par);

				// And return B which contains A (if it exists)
				return result;
			}
		}
	}
	throw std::invalid_argument("Missing matching closing parenthesis");
}



Opnode* checkOperator(std::string s, char op) {
	int op_index = s.find(op);
	if (op_index != -1) {
		Opnode *node = newNodeFromOperator(op);

		std::string arg0 = s.substr(0, op_index);
		std::string arg1 = s.substr(op_index + 1);

		if (arg0.length() == 0 || arg1.length() == 0) {
			throw std::invalid_argument("Operator missing an argument");
		}

		node->childA = nodeFromStr(s.substr(0, op_index));
		node->childB = nodeFromStr(s.substr(op_index + 1));
		return node;
	}
	return nullptr;
}

Opnode* nodeFromStr(std::string s) {
	Opnode *result;

	//Context context;
	//parseWhere(s, context);
	
	// Strip spaces
	int spi;
	while ((spi = s.find(" ")) != -1) {
		s = s.erase(spi, 1);
	}

	result = checkParenthesis(s);
	if (result != nullptr)
		return result;

	// Leading -
	if (s.find('-') == 0) {
		result = new node::Neg(nodeFromStr(s.substr(1)));
		return result;
	}

	// Operators
	for (int i=0; i < OPERATORS.length(); ++i) {
		char op = OPERATORS[i];
		result = checkOperator(s, op);
		if (result != nullptr)
			return result;
	}

	// Variables
	std::smatch m;
	std::regex e ("([a-zA-Z][\\w]*)");
	
	if (std::regex_match(s, e)) {
		return new node::Var(s);
	}

	if (std::regex_search(s, m, e)) {
		std::string news = std::string(m.prefix())
			.append("(")
			.append(m.str())
			.append(")")
			.append(m.suffix());
		printf("%s(%s)%s\n", m.prefix(), m.str(), m.suffix());
		return nodeFromStr(news);
	}

	// Literal
	result = new node::Const(strtod(s.c_str(), nullptr));
	return result;
}

Optree* parse(std::string s) {
	Context context;
	s = parseWhere(s, context);

	Optree *tree = new Optree();
	tree->setRoot(nodeFromStr(s));

	tree->context = context;

	return tree;
}
