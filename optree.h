#ifndef _OPTREE_h
#define _OPTREE_h

#include <string>
#include <math.h>
#include <map>

class Context {
	protected:
		std::map<std::string, double> variables; // TODO: Make variables be functions

	public:
		double getVarValue(std::string varName);
		void setVarValue(std::string varName, double value);
		bool varExists(std::string varName);
};

class Opnode {
	public:
		Opnode() {};
		Opnode(double arg) : arg(arg) {};
		Opnode(Opnode* child) : childA(child) {};
		Opnode(Opnode* childA, Opnode* childB) : childA(childA), childB(childB) {};
		Opnode(double arg, Opnode* childA, Opnode* childB) : arg(arg), childA(childA), childB(childB) {};
		
		double arg;
		Opnode* childA = nullptr;
		Opnode* childB = nullptr;
		virtual double execute(Context context) {
			return NAN;
		}
};

class Optree {
	public:
		Context context;

		Optree();
		~Optree();

		double execute();
		double execute(Context &context);
		void wipe();

		void setRoot(Opnode *node);
		Opnode* getRoot();

	private:
		Opnode* root;

		void _wipe(Opnode* leaf);
};

namespace node {

	class Const : public Opnode {
		using Opnode::Opnode;

		double execute(Context context) {
			return arg;
		}
	};

	class Var : public Opnode {
		protected:
			std::string id;

		public:
			Var(std::string varName) : id(varName) {}
			double execute(Context context) {
				return context.getVarValue(id);
			}
	};

	class Add : public Opnode {
		using Opnode::Opnode;

		double execute(Context context) {
			return childA->execute(context) + childB->execute(context);
		}
	};

	class Sub : public Opnode {
		using Opnode::Opnode;

		double execute(Context context) {
			return childA->execute(context) - childB->execute(context);
		}
	};

	class Mult : public Opnode {
		using Opnode::Opnode;

		double execute(Context context) {
			return childA->execute(context) * childB->execute(context);
		}
	};

	class Div : public Opnode {
		using Opnode::Opnode;

		double execute(Context context) {
			return childA->execute(context) / childB->execute(context);
		}
	};

	class Pow : public Opnode {
		using Opnode::Opnode;

		double execute(Context context) {
			return pow(childA->execute(context), childB->execute(context));
		}
	};

	class Mod : public Opnode {
		using Opnode::Opnode;

		double execute(Context context) {
			return fmod(childA->execute(context), childB->execute(context));
		}
	};

	class Sqrt : public Opnode {
		using Opnode::Opnode;

		double execute(Context context) {
			return sqrt(childA->execute(context));
		}
	};

	class Square : public Opnode {
		using Opnode::Opnode;

		double execute(Context context) {
			double a = childA->execute(context);
			return a * a;
		}
	};

	class Neg : public Opnode {
		using Opnode::Opnode;

		double execute(Context context) {
			return -childA->execute(context);
		}
	};
}

#endif
