/*
 * parser.cpp
 *
 * Created: 14/5/2017 20:04:48
 *  Author: Starkus
 */ 

#include <string>
#include "parser.h"


std::string operators = "+-*/^%";


opnode* newNodeFromOperator(char op) {
	switch (op)
	{
	case '+':
		return new node::sum();
	case '-':
		return new node::rest();
	case '*':
		return new node::mult();
	case '/':
		return new node::div();
	case '^':
		return new node::_pow();
	case '%':
		return new node::mod();
	}
}

bool strEndsWith(std::string str, std::string match) {
	return str.rfind(match) == str.length() - match.length();
}

#define OPERATOR(op)\
{\
	int op_index = s.find(op);\
	if (op_index != -1) {\
		opnode* node = newNodeFromOperator(op);\
		node->childA = parse(s.substr(0, op_index));\
		node->childB = parse(s.substr(op_index+1));\
		return node;\
	}\
}

#define FUNCTION(func)\
{\
	if (strEndsWith(pre_parenthesis, #func)) {\
		pre_parenthesis = pre_parenthesis.substr(0, pre_parenthesis.find(#func));\
		\
		solved_par = new node::_##func();\
		solved_par->childA = parse(inside_parenthesis);\
	}\
}

#define FUNCTION2(func)\
{\
	if (strEndsWith(pre_parenthesis, #func)) {\
		pre_parenthesis = pre_parenthesis.substr(0, pre_parenthesis.find(#func));\
		\
		int comma_index = inside_parenthesis.find(",");\
		\
		solved_par = new node::_##func();\
		solved_par->childA = parse(inside_parenthesis.substr(0, comma_index));\
		solved_par->childB = parse(inside_parenthesis.substr(comma_index + 1));\
	}\
}

#define VARIABLE(var, _node)\
{\
	int var_len = std::string(var).length();\
	int var_index = s.find(var);\
	\
	if (var_index != -1) {\
		std::string prevar = s.substr(0, var_index);\
		std::string postvar = s.substr(var_index + var_len);\
		\
		bool pre  =  prevar.length() > 0;\
		bool post = postvar.length() > 0;\
		\
		if (pre && post) {\
			opnode* a = new node::mult();\
			opnode* b = new node::mult();\
			\
			a->childA = parse(prevar);\
			a->childB = new node::_node();\
			\
			b->childA = a;\
			b->childB = parse(postvar);\
			\
			return b;\
		}\
		else if (pre) {\
			opnode* r = new node::mult();\
			r->childA = parse(prevar);\
			r->childB = new node::_node();\
			\
			return r;\
		}\
		else if (post) {\
			opnode* r = new node::mult();\
			r->childA = new node::_node();\
			r->childB = parse(postvar);\
			\
			return r;\
		}\
		return new node::_node();\
	}\
}


opnode* parse(std::string s) {

	// Parenthesis
	int begin_index = s.find('(');
	if (begin_index != -1) {
		
		std::string pre_parenthesis = s.substr(0, begin_index);
		std::string ss = s.substr(begin_index + 1);

		int levels = 0;

		for (int i=0; i < ss.length(); ++i) {
			
			if (ss[i] == '(')
				++levels;
			else if (ss[i] == ')')
				--levels;

			if (levels == -1) {
				
				std::string inside_parenthesis = ss.substr(0, i);
				std::string post_parenthesis = ss.substr(i + 1);

				opnode* solved_par = nullptr;

				char pre_op, post_op;

				
				FUNCTION(abs)
				FUNCTION(floor)
				FUNCTION(ceil)
				FUNCTION(round)
				FUNCTION(sqrt)
				FUNCTION(cbrt)
				FUNCTION(sinh)
				FUNCTION(cosh)
				FUNCTION(tanh)
				FUNCTION(asin)
				FUNCTION(acos)
				FUNCTION(atan)
				FUNCTION(asinh)
				FUNCTION(acosh)
				FUNCTION(atanh)
				FUNCTION(sin)
				FUNCTION(cos)
				FUNCTION(tan)
				FUNCTION(log2)
				FUNCTION(log)
				FUNCTION(ln)
				FUNCTION(exp)
				
				FUNCTION2(pow)
				FUNCTION2(atan2)
				FUNCTION2(hypot)


				if (solved_par == nullptr) {
					solved_par = parse(inside_parenthesis);
				}


				pre_op = pre_parenthesis[pre_parenthesis.length()-1];
				if (operators.find(pre_op) == -1) {
					pre_op = '*';
				} else {
					pre_parenthesis = pre_parenthesis.substr(0, pre_parenthesis.length()-1);
				}

				post_op = post_parenthesis[0];
				if (operators.find(post_op) == -1) {
					post_op = '*';
				} else {
					post_parenthesis = post_parenthesis.substr(1);
				}

				opnode *a, *b;

				bool pre  =  pre_parenthesis.length() > 0;
				bool post = post_parenthesis.length() > 0;

				// If we only got what's inside the parenthesis, just
				// parse that.
				if (!pre && !post) {
					return solved_par;
				}

				if (pre)
					a = newNodeFromOperator(pre_op);
				if (post)
					b = newNodeFromOperator(post_op);

				// If there's something before the parenthesis, fill
				// node A with (Pre, Inside).
				if (pre) {
					a->childA = parse(pre_parenthesis);
					a->childB = solved_par;
					// And if that's it, return A.
					if (!post)
						return a;

					// Else, we put A in a branch of B.
					b->childA = a;
				} else {
					// Or if only post-par, just the inside.
					b->childA = solved_par;
				}
				// Then the post-par in the other branch of B.
				b->childB = parse(post_parenthesis);

				// And return B which contains A (if it exists).
				return b;
			}
		}
	}
	
	OPERATOR('+')
	OPERATOR('-')
	OPERATOR('*')
	OPERATOR('/')
	OPERATOR('^')
	OPERATOR('%')

	// Variables
	
	VARIABLE("x", _x);
	VARIABLE("pi", _pi);
	VARIABLE("e", _e);

	// Literal
	opnode* node = new node::cons();
	node->arg = strtod(s.c_str(), NULL);
	return node;
}

double parseOnce(std::string s) {
	optree* tree = new optree;

	tree->makeFromstring(s);
	double result = tree->execute();

	delete tree;

	return result;
}
