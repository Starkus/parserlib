#include "optree.h"
#include "parser.h"
#include <stdio.h>
#include <iostream>

void printNode(Opnode *node, int level, int levels, Context &context, const char *prefix) {
	if (node == nullptr)
		return;

	for (int l=0; l < level; ++l) {
		if ((levels & 1<<l) != 0)
			printf("\033[1;9%dm│  \033[0m", (l%6)+1);
		else
			printf("   ");
	}
	printf("\033[1;9%dm%s\033[0m Node [%f]\n", (level%6)+1, prefix, node->execute(context));

	printNode(node->childA, level+1, levels | 1 << (level+1), context, "├─");
	printNode(node->childB, level+1, levels, context, "└─");
}

void printTree(Optree *tree) {
	printNode(tree->getRoot(), 0, 0, tree->context, "+─");
}

int main(int argc, char** argv) {
	Context context;

	std::string input;
	std::getline(std::cin, input);

	Optree *tree = parse(input);

	printTree(tree);

	printf("\n= %f\n", tree->execute());

	delete tree;
}

