#ifndef PARSER_h
#define PARSER_h

#include <string>
#include "optree.h"

Opnode *nodeFromStr(std::string);
Optree *parse(std::string);

#endif
