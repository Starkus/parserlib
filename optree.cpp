#include "optree.h"

void Context::setVarValue(std::string varName, double value) {
	variables[varName] = value;
}

double Context::getVarValue(std::string varName) {
	return variables[varName];
}

Optree::Optree() {
	root = nullptr;
}

Optree::~Optree() {
	wipe();
}

double Optree::execute() {
	return root->execute(context);
}

double Optree::execute(Context &context) {
	return root->execute(context);
}

void Optree::wipe() {
	_wipe(root);
}

void Optree::setRoot(Opnode* node) {
	root = node;
}

Opnode* Optree::getRoot() {
	return root;
}

void Optree::_wipe(Opnode *leaf) {
	if (leaf != nullptr) {
		_wipe(leaf -> childA);
		_wipe(leaf -> childB);
		delete leaf;
	}
}

